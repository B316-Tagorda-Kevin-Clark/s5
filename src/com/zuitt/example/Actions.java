package com.zuitt.example;

public interface Actions {
    //Interfaces are used to achieve abstraction. It hides away the actual implementation of the methods and simply show a "list" / "menu" of methods that can be done.
    //Interfaces are like blueprints for your classes. Because any class that implements our interface MUST have the methods in the interface
    public void sleep();
    public void run();

    public void eat();

    public void shower();
}
