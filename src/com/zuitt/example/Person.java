package com.zuitt.example;

public class Person implements Actions {
    //For a class to implement or use an interface, we use the implements keyword

    public void sleep() {
        System.out.println("ZZZZZZ");
    }

    public void run() {
        System.out.println("Running on the road");

    }

    public void eat() {
        System.out.println("Run to the nearest Mcdo and buy burger with fries");
    }

    public void shower() {
        System.out.println("Use safeguard");
    }
}
