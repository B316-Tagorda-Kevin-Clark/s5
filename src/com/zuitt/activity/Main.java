package com.zuitt.activity;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        ArrayList<String> contact1Numbers = new ArrayList<>();
        contact1Numbers.add("11111111111");
        contact1Numbers.add("11111111112");

        Contact contact1 = new Contact("Berto", contact1Numbers, "B1L4 Maserati St", "Mckinley");

        ArrayList<String> contact2Numbers = new ArrayList<>();
        contact2Numbers.add("22222222222");
        contact2Numbers.add("22222222223");

        Contact contact2 = new Contact("Vintage", contact2Numbers, "B1l4 Maserati St", "Technohub");

        phonebook.getContacts().add(contact1);
        phonebook.getContacts().add(contact2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Your Phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("-------------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                for (String number : contact.getContactNumbers()) {
                    System.out.println("+" + number);
                }
                System.out.println("-------------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println("My home in " + contact.getHomeAddress());
                System.out.println("My office in " + contact.getOfficeAddress());
                System.out.println("========================");
            }
        }
    }
}
