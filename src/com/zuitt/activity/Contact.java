package com.zuitt.activity;

import java.util.ArrayList;

public class Contact {
    private String name;
    private ArrayList<String> contactNumbers;
    private String homeAddress;
    private String officeAddress;

    public Contact() {
        contactNumbers = new ArrayList<>();
    }

    public Contact(String name, ArrayList<String> contactNumbers, String homeAddress, String officeAddress) {
        this.name = name;
        this.contactNumbers = contactNumbers;
        this.homeAddress = homeAddress;
        this.officeAddress = officeAddress;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getContactNumbers() {
        return contactNumbers;
    }
    public void setContactNumbers(ArrayList<String> contactNumbers) {
        this.contactNumbers = contactNumbers;
    }

    public String getHomeAddress() {
        return homeAddress;
    }
    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }
    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }
}
